<!DOCTYPE html>
<html lang="en" ng-app="slack">
<head>
    <base href="/" />
    <meta charset="UTF-8" />
    <meta name="viewport" content="initial-scale=1" />

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="Evening Raiders Slack Self Invite">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="#0D47A1">
    <meta name="apple-mobile-web-app-title" content="Evening Raiders Slack Self Invite">
    <!-- <meta name="msapplication-TileImage" content="img/touch/ms-touch-icon-144x144-precomposed.png"> -->
    <meta name="msapplication-TileColor" content="#0D47A1">
    <meta name="theme-color" content="#0D47A1">

    <title>Evening Raiders on Slack</title>

    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/angular_material/0.11.0/angular-material.min.css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=RobotoDraft:300,400,500,700,400italic" />
    <link rel="stylesheet" href="css/style.css" />
</head>
<body>
    <section id="main" layout="column" flex ui-view=""></section>

    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.2.15/angular-ui-router.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular-animate.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular-aria.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular-messages.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angular_material/0.11.0/angular-material.min.js"></script>

    <script src="js/app.min.js"></script>
</body>
</html>
