!function () {
  'use strict';

  angular.module('app.core').config(routeConfig);

  /* ngInject */
  function routeConfig($locationProvider, $urlRouterProvider, $stateProvider) {
    $locationProvider.html5Mode(true);
    $urlRouterProvider.otherwise('/');

    $stateProvider.state('app', {
      url: '',
      abstract: true,
      templateUrl: 'views/app.html',
      controller: 'CoreController',
      controllerAs: 'core'
    })
    .state('app.home', {
      url: '/',
      templateUrl: 'views/home/index.html'
    });
  }
}();
