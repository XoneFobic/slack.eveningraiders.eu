!function () {
  'use strict';

  angular.module('app.core').controller('CoreController', coreController);

  /* ngInject */
  function coreController($http, $mdSidenav, $mdToast, $window) {
    var vm = this;

    vm.credentials = {};

    vm.toggleSidenav = function (menuId) {
      $mdSidenav(menuId).toggle();
    };

    vm.submit = function() {
      $http.post('/api/invite', vm.credentials).then(function (response) {
        $window.location.href = response.data.redirect;
      }, function(response) {
        console.log(response.data.error);
        $mdToast.show($mdToast.simple().content(response.data.error).position('top right').theme('error-toast'));
      });
    };
  }
}();
