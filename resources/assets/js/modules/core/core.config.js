!function () {
  'use strict';

  angular.module('app.core').config(coreConfig);

  /* ngInject */
  function coreConfig($mdThemingProvider) {
    $mdThemingProvider.theme('default')
      .primaryPalette('blue', {
        'default': '900'
      })
      .accentPalette('amber', {
        'default': 'A200'
      });
  }
}();
