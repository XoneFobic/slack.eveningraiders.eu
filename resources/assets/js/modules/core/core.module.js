!function () {
  'use strict';

  angular.module('app.core', [
    'ui.router',
    'ui.router.router',
    'ngMaterial',
    'ngMessages'
    ]);
}();
