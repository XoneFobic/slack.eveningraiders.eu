<?php

return [
    'already_invited'     => 'Already Invited.',
    'invalid_credentials' => 'Invalid Credentials.',
    'sent_recently'       => 'This email address has recently been invited.'
];
