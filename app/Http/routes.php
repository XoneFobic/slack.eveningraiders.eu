<?php

$app->post('/api/invite', 'InviteController@store');

// $app->get('/{any:[.+]+}', function () use ($app) {
$app->get('/{any:[.+]?}', function () use ($app) {
    return view('master');
});
