<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class InviteController extends Controller
{
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function store(Request $request)
    {
        $rules = [
            'email'    => 'required|email',
            'password' => 'required'
        ];

        $this->validate($request, $rules);

        if ($request->input('password') !== env('SLACK_API_PASSWORD')) {
            return response()->json(['error' => trans('slack.invalid_credentials')], 422);
        }

        // Send API request to Slack
        $token    = getenv('SLACK_API_KEY');
        $teamname = getenv('TEAM_NAME');
        $email    = $request->input('email');
        $slackUrl = getenv('TEAM_NAME_URL');

        $response = $this->client->post("https://{$slackUrl}.slack.com/api/users.admin.invite?token={$token}&email={$email}");

        $responseBody = (object) json_decode($response->getBody());

        if (! $responseBody->ok) {
            return response()->json(['error' => trans('slack.' . $responseBody->error)], 422);
        }
        //

        return response()->json(['ok' => $responseBody->ok, 'redirect' => "https://{$slackUrl}.slack.com"]);
    }
}
